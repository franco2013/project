/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerServletTwo;

import entityTest.Club;
import entityTest.Member1;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessionTest.ClubFacade;
import sessionTest.Member1Facade;

@WebServlet(name="ControllerServletTwo",loadOnStartup = 1,urlPatterns = {"/testViewTwo","/index","/regPage","/playerStats","/logonPage"})

public class controllerServletTwo extends HttpServlet
{
      @EJB
      private ClubFacade anyClubFacade;
      
      @EJB
      private Member1Facade anyMember1Facade;
      
      
      
      
      
      //@EJB
      //private OrderManager anyOrderManager;
     private List  clubList = new ArrayList();   
             
      @Override
      public void init() throws ServletException
      {
          
        //  getServletContext().setAttribute("clubs",anyClubFacade.findAll());
          
      }//end init method
    
   /* protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");*/     
       @Override
        protected void doGet (HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
        {
            
        String userPath = request.getServletPath();
        
        // if testViewTwo page is requested
        if (userPath.equals("/testViewTwo")) 
        {
               
         //   userPath="/index";
                                                            //here we are persisting club data to the database
            String ClubID = request.getParameter("clubID");    
            
            int anyPhoneNo = Integer.parseInt(request.getParameter("phoneNo"));
            
            String Address = request.getParameter("address");
            String Name = request.getParameter("name");
            
            Club theClub = new Club(ClubID, anyPhoneNo, Address, Name);
            
            anyClubFacade.create(theClub);
            
          //  PrintStream printf = System.out.printf("This is controllerServlet/n The name is %s",Name);
     // anyOrderManager.addClubToDatabase(ClubID, anyPhoneNo, Address, Name);          
             
            
        }else if (userPath.equals("/DisplayClubTable"))
        {
             clubList = anyClubFacade.findAll();
            request.setAttribute("clubList", clubList);
            userPath="/DisplayClubTable";
         //  request.setAttribute("clubs",anyClubFacade.findAll());    
          
          
            
        }else if (userPath.equals("/regPage"))
        {
            
            String name = request.getParameter("firstname");          
            String lastname= request.getParameter("lastname");            
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String email = request.getParameter("email");
           // String county = request.getParameter("county");
            String addressiMAGE = request.getParameter("address");
           int anyPhoneNo = Integer.parseInt(request.getParameter("phoneno"));
          // int memId = 12;
           
         
           Member1 anyMember = new Member1(name,lastname,email,anyPhoneNo,password,username,addressiMAGE);
           anyMember1Facade.create(anyMember); 
        
        
        } else if (userPath.equals("/logonPage"))
        { 
            
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            
           
             userPath="/testViewPage.jsp";
            
            
        }
        
          // use RequestDispatcher to forward request internally
        String url = "/WEB-INF" + userPath;
     //    System.out.printf(url);
        
        try {
        request.getRequestDispatcher(url).forward(request, response);
        } 
        catch (Exception ex)
        {
            System.out.printf("Not Dispactched",ex.toString());
        } 
    }   //end method doGet
       
    

   
       
       
       
       
                
}   //end class controllerServletTwo

