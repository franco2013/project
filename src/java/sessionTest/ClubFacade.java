/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionTest;

import entityTest.Club;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author franks
 */
@Stateless
public class ClubFacade extends AbstractFacade<Club> {
    @PersistenceContext(unitName = "virtualCoachTwoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClubFacade() {
        super(Club.class);
    }
    
}
