/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionTest;

import entityTest.Member1;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author franks
 */
@Stateless
public class Member1Facade extends AbstractFacade<Member1> {
    @PersistenceContext(unitName = "virtualCoachTwoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Member1Facade() {
        super(Member1.class);
    }
    
}
