package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class PlayerStatsPage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/playerStats.css\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("       </html>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t<header>   \n");
      out.write("\t\n");
      out.write("\t\t<link  rel=\"stylesheet\"   text=\"text/css\" href=\"playerStatsPage.css\">\n");
      out.write("\t\n");
      out.write("\t</header>\n");
      out.write("\t\n");
      out.write("\t\n");
      out.write("\t\t\n");
      out.write("\t<body id=\"anyBody\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t\t<div id=\"pagewidth\">\n");
      out.write("\n");
      out.write("\t\t\t\t\t<div id=\"header\">\t\t\n");
      out.write("\t\t\t\t\t   Header...\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t<div id=\"nav\"> \n");
      out.write("\t\t\t\t\t\t\t\tNav...\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div id=\"sideNav\">\n");
      out.write("\t\t\t\t\t            Side Nav...\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\t<div id=\"content\">\t\n");
      out.write("                                   Content...\n");
      out.write("\t\t\t\t\t\t\t\t   \n");
      out.write("\t\t\t\t\t\t<form name=\"playerStats\" method=\"get\" action=\"...????.....\" >\t\t   \n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"tableRow\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p> Name:</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t<p><input type=\"text\" name=\"name\" value=\"\"></p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"tableRow\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>Phone No:</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p><input type=\"text\" name=\"phoneNo\" value=\"\"></p>\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"tableRow\">\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t<p>Height:</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p><input type=\"text\" name=\"height\" value=\"\"></p>\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"tableRow\">\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>Position:</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p><select name=\"position\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"goalkeeper\">goalkeeper</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"defender\">defender</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"midfielder\">midfielder</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"attacker\">attacker</option>\t\t\t\t\t\t\t\t\t\t \n");
      out.write("\t\t\t\t\t\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t\t\t\t\t\t</p>\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"tableRow\">\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>Goals Scored:</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p> \n");
      out.write("\t\t\t\t\t\t\t\t\t<select name=\"goalsScored\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"01\">01</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"02\">02</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"03\">03</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"04\">04</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"05\">05</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"06\">06</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"07\">07</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"08\">08</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"09\">09</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"10\">10</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"11\">11</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"12\">12</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"13\">13</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"14\">14</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"15\">15</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"16\">16</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"17\">17</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"18\">18</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"19\">19</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"20\">20</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t\t\t\t\t\t</p>\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"tableRow\">\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>Start Season</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p> <select name=\"seasonStarted\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"2008\">2008</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"2009\">2009</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"2010\">2010</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"2011\">2011</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"2012\">2012</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t  <option value=\"2013\">2013</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t\t\t\t\t\t</p>\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"tableRow\">\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>Foot:</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<p> <input type=\"radio\" name=\"foot\" value=\"right\"> Right<br>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"foot\" value=\"left\"> Left<br>\t\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"foot\" value=\"both\"> Both\n");
      out.write("\t\t\t\t\t\t\t\t     </p>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   \n");
      out.write("\t\t\t\t\t\t\t\t   \n");
      out.write("\t\t\t\t\t\t\t</form>\t   \n");
      out.write("\t\t\t\t\t\t\t\t   \n");
      out.write("\t\t\t\t\t\t\t\t   \n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\t<div id=\"clear\">\t\n");
      out.write("\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\n");
      out.write("                       <div id=\"footer\">\t\n");
      out.write("                                 footer...\n");
      out.write("\t\t\t\t\t   </div>\n");
      out.write("\n");
      out.write("\t\t\t</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
