<%-- 
    Document   : RegitrationPage
    Created on : Feb 17, 2013, 5:49:40 PM
    Author     : franks
--%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/RegistrationPage.css">
        <title>JSP Page</title>
    </head>
    <body>     
	
		
	<body id="anyBody">


			<div id="pagewidth">

					<div id="header">		
					   Header...
					</div>

					<div id="nav"> 
								Nav...
					</div>
					<div id="sideNav">
					            Side Nav...
					</div>
					
					<div id="content">	
                                   Content...
									   
						<form name="regpage" method="get" action="<c:url value= 'regPage'/>" >		   
							
								<div class="tableRow">
									
									<p> FirstName:</p>
									<p><input type="text" name="firstname" value=""></p>
									
								
								</div>
								<div class="tableRow">
									
									<p> LastName:</p>
									<p><input
									type="text" name="lastname" value=""></p>									
								
								</div>
								<div class="tableRow">
									
									<p> UserName:</p>
									<p><input type="text" name="username" value=""></p>
									
								
								</div>
								<div class="tableRow">
									
									<p>Password:</p>
									
									<p><input type="text" name="password" value=""></p>						
								
								</div>
								<div class="tableRow">
									
									<p> Phone No:</p>
									
									<p><input type="text" name="phoneNo" value=""></p>					
								
								</div>
								
								<div class="tableRow">
									
									<p> Email:</p>
									
									<p><input type="text" name="email" value=""></p>					
								
								</div>			
								<div class="tableRow">
									
									<p> Address:</p><br>									
									
										<textarea name="comments"></textarea>
									</p>				
								
								</div>
								
								<div class="tableRow">							
								
									
									<p><input type="submit" value="Submit"></p>					
								
								</div>
															   
								   
							</form>	   
								   
								   
					</div>
					
					<div id="clear">	

					</div>
					
                       <div id="footer">	
                                 footer...
					   </div>

			</div>


	</body>
   
</html>
